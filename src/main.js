import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import 'material-design-icons/iconfont/material-icons.css'
import axios from 'axios'
import VueSession from 'vue-session';

Vue.use(VueSession);

Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.prototype.$directory = 'http://localhost/laravel_proj/public'

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
