import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// my routes
const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('@/views/Login.vue'),
    meta: {
      title: 'HMIS: Login'
    }
  },
  {
    path: '/pre',
    name: 'pre-pregister',
    component: () => import('@/views/Pre_register.vue'),
    meta: {
      title: 'About'
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('@/views/Register.vue'),
    meta: {
      title: 'HMIS: Register'
    }
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('@/views/main/Home.vue'),
    meta: {
      title: 'HMIS: Home'
    }
  },
  {
    path: '/playground',
    name: 'Playground',
    component: () => import('@/views/Playground.vue'),
    meta: {
      title: 'HMIS: Playground'
    }
  }
]

// variable for my router
const router = new VueRouter({
  mode: 'history',
  routes
})

// export my routes
export default router
